package com.agero.nccsdk.internal.data.preferences;

import com.agero.nccsdk.adt.detection.start.model.WifiActivity;
import com.agero.nccsdk.internal.notification.model.NccNotification;

/**
 * Created by james hermida on 10/24/17.
 */

public interface SharedPrefs {

    void setUserId(String userId);

    String getUserId();

    void setRecentWifiActivity(WifiActivity lastWifiConnectionInfo);

    WifiActivity getRecentWifiActivity();

    void saveNotification(NccNotification notification);

    NccNotification getNotification(String id);

    void setLbtAutoRestart(boolean bool);

    boolean shouldLbtAutoRestart();

    void setAuthenticatedApiKey(String apiKey);

    String getAuthenticatedApiKey();

    void setAccessId(String accessId);

    String getAccessId();
}
