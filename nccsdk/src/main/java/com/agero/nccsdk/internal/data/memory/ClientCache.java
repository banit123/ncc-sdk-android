package com.agero.nccsdk.internal.data.memory;

import com.agero.nccsdk.TrackingContext;

/**
 * Created by james hermida on 2/8/18.
 */

public interface ClientCache {

    /**
     * Sets the context that will be associated with the data collected by the SDK.
     *
     * @param trackingContext
     */
    void setTrackingContext(TrackingContext trackingContext);

    /**
     * Gets the context associated with the data collected by the SDK.
     *
     * @return
     */
    TrackingContext getTrackingContext();

    /**
     * Clears the context
     */
    void clearTrackingContext();
}
