package com.agero.nccsdk.internal.data.network.rest.apigee.model;

public class AuthUserRequest extends ApigeeRequest {

    private String appId;

    public AuthUserRequest(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }
}
