package com.agero.nccsdk;

/**
 * Created by james hermida on 8/10/17.
 */

final class NccSdkVersion {
    static final String BUILD = "0.1.8";
}
