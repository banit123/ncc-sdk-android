package com.agero.nccsdk.ubi.collection.mapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by james hermida on 10/27/17.
 */

public abstract class AbstractSensorDataMapper implements SensorDataMapper {

    private final SimpleDateFormat SDF_READABLE_DATE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ", Locale.US);
    private final Date date = new Date();
    final StringBuilder sb = new StringBuilder();

    protected String getReadableTimestamp(long timeMillis) {
        date.setTime(timeMillis);
        return SDF_READABLE_DATE.format(date);
    }

    void clearStringBuilder() {
        sb.delete(0, sb.length());
    }
}
