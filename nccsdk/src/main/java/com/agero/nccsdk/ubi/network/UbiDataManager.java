package com.agero.nccsdk.ubi.network;

import android.content.Context;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.internal.common.io.FileCompressor;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.common.util.Clock;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.data.network.aws.s3.FileTransferManager;
import com.agero.nccsdk.internal.di.qualifier.UbiDataPath;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.ubi.UbiState;
import com.agero.nccsdk.ubi.UbiStateMachine;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UbiDataManager implements UbiSyncManager {

    private final Context context;
    private final Executor executor;
    private final FileTransferManager fileTransferManager;
    private final File ubiDataPath;
    private final Clock clock;
    private final StateMachine<UbiState> ubiStateMachine;

    @Inject
    public UbiDataManager(Context context, Executor executor, Clock clock, FileTransferManager fileTransferManager, @UbiDataPath File dataPath, StateMachine<UbiState> ubiStateMachine) {
        this.context = context;
        this.executor = executor;
        this.clock = clock;
        this.fileTransferManager = fileTransferManager;
        this.ubiDataPath = dataPath;
        this.ubiStateMachine = ubiStateMachine;
    }

    @Override
    public void uploadSession(File file) {
        if (FileUtils.isNullOrDoesNotExist(file)) {
            Timber.e("uploadSession() - File is null or does not exist");
        } else {
            fileTransferManager.uploadFile(getUploadS3Bucket(), file);
        }
    }

    private String getUploadS3Bucket() {
        return BuildConfig.S3_BUCKET_UBI + "/" + getS3DatePath();
    }

    private String getS3DatePath() {
        Date date = new Date(clock.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/d", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    @Override
    public void uploadAllSessions() {
        Timber.d("Sync requested");
        boolean syncRequested = false;

        // If the UBI data path is null or does not exist,
        // then no trips have ever occurred on the device.
        // After the first trip occurs, the data path
        // will always exist
        if (!FileUtils.isNullOrDoesNotExist(ubiDataPath)) {
            File[] files = ubiDataPath.listFiles();

            // Iterate through files
            for (File file : files) {
                String fileName = file.getName();
                if (isCurrentSession(fileName)) {
                    Timber.d("Skipping current session file for sync: %s", file.getAbsolutePath());
                } else if (file.isDirectory() && isValidUuid(fileName)) { // session data requires compression
                    executor.execute(getProcessSessionDataRunnable(file)); // extract instance creation to method for mocking
                    syncRequested = true;
                } else if (!file.isDirectory() && fileName.endsWith(FileCompressor.EXTENSION_TAR)) { // session data already compressed
                    String fileNameExcludingExt = fileName.substring(0, fileName.lastIndexOf(FileCompressor.EXTENSION_TAR));
                    if (DeviceUtils.isConnectedToWiFi(context)) {
                        Timber.d("Starting upload for session id: %s", fileNameExcludingExt);
                        uploadSession(file);
                    } else {
                        Timber.d("Skipping upload due to no wifi connection for session id: %s", fileNameExcludingExt);
                    }
                    syncRequested = true;
                } else {
                    Timber.w("Skipping file for sync: %s", file.getAbsolutePath());
                }
            }
        }

        if (!syncRequested) {
            Timber.d("No pending data to sync");
        }
    }

    private boolean isValidUuid(String uuid) {
        String REGEX_UUID = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";
        return Pattern.matches(REGEX_UUID, uuid);
    }

    private ProcessSessionDataRunnable getProcessSessionDataRunnable(File file) {
        return new ProcessSessionDataRunnable(file);
    }

    // TODO reconsider implementation: what if other components want the current session id?
    private boolean isCurrentSession(String id) {
        return ((UbiStateMachine) ubiStateMachine).getCurrentSessionId().equalsIgnoreCase(id);
    }
}
