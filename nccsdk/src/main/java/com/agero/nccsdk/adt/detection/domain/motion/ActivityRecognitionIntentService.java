package com.agero.nccsdk.adt.detection.domain.motion;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.ActivityRecognitionResult;

public class ActivityRecognitionIntentService extends IntentService {

    public static final String ACTIVITY_RESULT = "ACTIVITY_RESULT";
    public static final String ACTION_ACTIVITY_UPDATE = "ACTION_ACTIVITY_UPDATE";

    public ActivityRecognitionIntentService() {
        super(ActivityRecognitionIntentService.class.getName());
    }

    /**
     * Called when a new activity detection update is available.
     */
    @SuppressLint("LongLogTag")
    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

            Intent i = new Intent(ACTION_ACTIVITY_UPDATE);
            i.putExtra(ACTIVITY_RESULT, result);
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
            manager.sendBroadcast(i);
        } else {
            Timber.w("Intent contains no activity recognition result");
        }
    }
}
