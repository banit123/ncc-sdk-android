package com.agero.nccsdk.adt.detection;

/**
 * Created by james hermida on 11/14/17.
 *
 * Strategy pattern to evaluate data
 *
 */

public interface EvaluateStrategy<T> {

    void evaluate(T data);

}
