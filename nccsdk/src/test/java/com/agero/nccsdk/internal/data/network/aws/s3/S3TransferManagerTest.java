package com.agero.nccsdk.internal.data.network.aws.s3;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Constructor;

import io.reactivex.Single;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
public class S3TransferManagerTest {

    private S3TransferManager s3TransferManager;

    @Mock
    private TransferUtility mockTransferUtility;

    @Mock
    private TransferObserver mockTransferObserver;

    @Mock
    private AuthManager mockAuthManager;

    @Before
    public void setUp() throws Exception {
        Constructor[] ctors = S3TransferManager.class.getDeclaredConstructors();
        s3TransferManager = PowerMockito.spy((S3TransferManager) ctors[0].newInstance(mockTransferUtility, mockAuthManager));
    }

    @Test
    @PrepareForTest({FileUtils.class})
    public void uploadFile_has_authenticated() throws Exception {
        String testTarget = "target";
        File testFile = new File("");
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.doReturn(false).when(FileUtils.class, "isNullOrDoesNotExist", testFile);
        when(mockTransferUtility.upload(testTarget, testFile.getName(), testFile)).thenReturn(mockTransferObserver);

        PowerMockito.doReturn(Single.just(true)).when(mockAuthManager).hasAuthenticated();

        s3TransferManager.uploadFile(testTarget, testFile);
        verify(mockTransferUtility).upload(testTarget, testFile.getName(), testFile);
    }

    @Test
    @PrepareForTest({FileUtils.class})
    public void uploadFile_has_not_authenticated() throws Exception {
        String testTarget = "target";
        File testFile = new File("");
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.doReturn(false).when(FileUtils.class, "isNullOrDoesNotExist", testFile);
        when(mockTransferUtility.upload(testTarget, testFile.getName(), testFile)).thenReturn(mockTransferObserver);

        PowerMockito.doReturn(Single.just(false)).when(mockAuthManager).hasAuthenticated();

        s3TransferManager.uploadFile(testTarget, testFile);
        verify(mockTransferUtility, times(0)).upload(testTarget, testFile.getName(), testFile);
    }

    @Test
    @PrepareForTest({StringUtils.class})
    public void uploadFile_null_or_empty_destination() {
        String testTarget = null;
        s3TransferManager.uploadFile(testTarget, null);
        verifyZeroInteractions(mockTransferUtility);

        testTarget = "";
        s3TransferManager.uploadFile(testTarget, null);
        verifyZeroInteractions(mockTransferUtility);
    }

    @Test
    @PrepareForTest({FileUtils.class})
    public void uploadFile_null_file() throws Exception {
        String testTarget = "target";
        File testFile = null;
        PowerMockito.mockStatic(FileUtils.class);
        when(FileUtils.class, "isNullOrDoesNotExist", testFile).thenReturn(true);
        s3TransferManager.uploadFile(testTarget, testFile);
        verifyNoMoreInteractions(mockTransferUtility);
    }

    @Test
    @PrepareForTest({FileUtils.class, S3TransferManager.class})
    public void uploadFile_file_already_queued() throws Exception {
        String testTarget = "target";
        File testFile = new File("");
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.doReturn(false).when(FileUtils.class, "isNullOrDoesNotExist", testFile);
        PowerMockito.doReturn(true).when(s3TransferManager, "hasFileAlreadyBeenQueuedForUpload", testFile);

        s3TransferManager.uploadFile(testTarget, testFile);
        verifyZeroInteractions(mockTransferUtility);
    }
}
