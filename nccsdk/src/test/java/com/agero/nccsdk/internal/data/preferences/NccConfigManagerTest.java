package com.agero.nccsdk.internal.data.preferences;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccBarometerConfig;
import com.agero.nccsdk.domain.config.NccBatteryConfig;
import com.agero.nccsdk.domain.config.NccGyroscopeConfig;
import com.agero.nccsdk.domain.config.NccGyroscopeUncalibratedConfig;
import com.agero.nccsdk.domain.config.NccLinearAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.config.NccMagneticFieldConfig;
import com.agero.nccsdk.domain.config.NccMagneticFieldUncalibratedConfig;
import com.agero.nccsdk.domain.config.NccMotionConfig;
import com.agero.nccsdk.domain.config.NccRotationVectorConfig;
import com.agero.nccsdk.lbt.config.KinesisConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Constructor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 10/19/17.
 */

@RunWith(PowerMockRunner.class)
public class NccConfigManagerTest {

    NccConfigManager nccConfigManager;

    @Mock
    NccSharedPrefs mockSharedPrefs;

    @Before
    public void setUp() throws Exception {
        Constructor[] ctors = NccConfigManager.class.getDeclaredConstructors();
        nccConfigManager = (NccConfigManager) ctors[0].newInstance(mockSharedPrefs);
    }

    @Test
    public void saveConfig_valid_input() throws Exception {
        NccSensorType testSensorType = NccSensorType.ACCELEROMETER;
        NccAccelerometerConfig testConfig = new NccAccelerometerConfig();
        Gson gson = new Gson();

        nccConfigManager.saveConfig(testSensorType, testConfig);
        verify(mockSharedPrefs).put(testSensorType.getName(), gson.toJson(testConfig));
    }

    @Test
    public void saveConfig_null_config() throws Exception {
        NccSensorType testSensorType = NccSensorType.ACCELEROMETER;
        NccAccelerometerConfig testConfig = null;

        nccConfigManager.saveConfig(testSensorType, testConfig);
        verifyZeroInteractions(mockSharedPrefs);
    }

    @Test
    public void getConfig() throws Exception {
        NccSensorType testSensorType = NccSensorType.LOCATION;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccLocationConfig);

        testSensorType = NccSensorType.MOTION_ACTIVITY;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccMotionConfig);

        testSensorType = NccSensorType.ACCELEROMETER;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccAccelerometerConfig);

        testSensorType = NccSensorType.LINEAR_ACCELEROMETER;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccLinearAccelerometerConfig);

        testSensorType = NccSensorType.GYROSCOPE;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccGyroscopeConfig);

        testSensorType = NccSensorType.GYROSCOPE_UNCALIBRATED;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccGyroscopeUncalibratedConfig);

        testSensorType = NccSensorType.MAGNETIC_FIELD;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccMagneticFieldConfig);

        testSensorType = NccSensorType.MAGNETIC_FIELD_UNCALIBRATED;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccMagneticFieldUncalibratedConfig);

        testSensorType = NccSensorType.ROTATION_VECTOR;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccRotationVectorConfig);

        testSensorType = NccSensorType.BAROMETER;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccBarometerConfig);

        testSensorType = NccSensorType.BATTERY;
        assertTrue(nccConfigManager.getConfig(testSensorType) instanceof NccBatteryConfig);
    }

    @Test
    public void saveSdkConfig_valid_input() throws Exception {
        SdkConfigType testSensorType = SdkConfigType.KINESIS;
        KinesisConfig testConfig = new KinesisConfig(1000);
        Gson gson = new Gson();

        nccConfigManager.saveSdkConfig(testSensorType, testConfig);
        verify(mockSharedPrefs).put(testSensorType.getName(), gson.toJson(testConfig));
    }

    @Test
    public void saveSdkConfig_null_config() throws Exception {
        SdkConfigType testSensorType = SdkConfigType.KINESIS;
        KinesisConfig testConfig = null;

        nccConfigManager.saveSdkConfig(testSensorType, testConfig);
        verifyZeroInteractions(mockSharedPrefs);
    }

    @Test
    public void getSdkConfig() throws Exception {
        SdkConfigType testSdkType = SdkConfigType.KINESIS;
        assertTrue(nccConfigManager.getSdkConfig(testSdkType) instanceof KinesisConfig);
    }
}
